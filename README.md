# (B)iggest (Sq)uare

Gitlab Mirror for BSQ project in 42KL February C Piscine 
This project asks to fill in the biggest square found in a map with obstacles and print the result.
Input can be read from multiple files or through standard input.

A map should specify:
1. On the first line, number of lines, free character, obstacle character, fill character.
2. For the next n lines, the contents of the map followed by a line break.
Example: 
```
9.ox
.....
....o
...o.
...oo
.....
.....
.o...
..o..
o..o.
```

## Getting Started

After cloning the repository, open a terminal in the root of the repository.
Run the makefile using `make re` to recompile the binary `bsq`.

To read from file(s), run
```
./bsq <filename1> <filename2> ...
```

To read from standard input, simply run `./bsq`

## Running the tests

Maps can be generated using the perl script `map_gen` provided.

To generate maps and save to file, simply run:
```
perl map_gen <num_col> <num_rows> <obstacle_density> > <filename>
```

Some sample maps are saved in the /maps directory for testing.

The perl script uses the character set `.ox` by default, but can be modified to generate maps using other character sets as well.

## Acknowledgments

* GeeksForGeeks for algorithm logic
* My fellow BSQ groupmates for the support and help
* 42KL and 42 for the opportunity
